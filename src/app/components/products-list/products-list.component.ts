import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/products.service';
import { Product } from 'src/app/services/Product';
import { delay } from 'rxjs/operators';
//https://controlc.com/d2916190
@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnInit {
  //NON si mette product vuoto nel costruttore
  // perché non c’è nulla, lasciamo undefined
  products?: Product[]; // sono undefined
  isloading?: boolean; // sono undefined

  constructor(private productService: ProductService) {}

  ngOnInit(): void {
    this.isloading = true;
    //inizio a caricare i dati di isloading e product
    this.productService
      .fetchProducts()
      .pipe(delay(2000))
      .subscribe((products: Product[]) => {
        // risultati
        this.products = products;
        this.isloading = false;
      });
  }
}
