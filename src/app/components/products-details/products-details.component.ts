import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/products.service';
import { Product } from 'src/app/services/Product';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-products-details',
  templateUrl: './products-details.component.html',
  styleUrls: ['./products-details.component.scss'],
})
export class ProductsDetailsComponent implements OnInit {
  product?: Product;

  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    //subscribe perchè mi serve il dato, prende il dato e rimane in ascolto
    this.activatedRoute.params.subscribe((params) => {
      this.productService
        .fetchProductById(params['id'])
        .subscribe((fetchedProduct: Product) => {
          this.product = fetchedProduct;
        });
    });
    console.log(this.product);
  }
}
