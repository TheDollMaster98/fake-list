import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Product } from 'src/app/services/Product';
import { Validator } from '@angular/forms';
@Component({
  selector: 'app-products-form',
  templateUrl: './products-form.component.html',
  styleUrls: ['./products-form.component.scss'],
})
export class ProductsFormComponent {
  // creo una variabile di tipo FormGroup
  newProductForm: FormGroup;

  constructor() {
    //inizializzo la formGroup con i valori di default azzerati
    this.newProductForm = new FormGroup({
      id: new FormControl(null, [Validators.required, Validators.min(0)]),

      title: new FormControl('', [
        Validators.required, // validazione obbligatoria
        Validators.minLength(3),
        Validators.maxLength(150),
      ]),

      price: new FormControl(0, [Validators.required, Validators.min(0)]),

      description: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(500),
      ]),

      category: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ]),
    });
  }

  updateToCart() {
    // stampo i dati del form
    console.log(this.newProductForm.value);
    // resetta la form di nuovo
    this.newProductForm.reset();
    //manca la parte di validazione
  }
}
