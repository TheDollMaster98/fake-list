//product è un’interfaccia che semplicemente è un oggetto con le caratteristiche
export interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  image: string;
  category?: string; // indica che il campo è opzionale
}

// non va in una cartella separata, in un progetto grande tipicamente, i tipi (interfacce, classi, ecc)
// vanno messe dove vengono utilizzate, difianco i serice, perché tendenzialmente prooducano il prodotto
// anche dentro la cartella services, dentro app cartella con services + interfaces
// se no mettere la cartella interface in services
