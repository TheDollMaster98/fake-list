//da rinominare
import { Injectable } from '@angular/core';
//per testare il fetch, observable mi serve per vedere i dati del fetch, e con il subscribe lo faccio partire
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
//import { delay } from 'rxjs/operators'; //da mettere sul subscribe del products-list
import { Product } from './Product';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private httpClient: HttpClient) {}
  //con <> indico la tipizzazione
  fetchProducts(): Observable<Product[]> {
    return this.httpClient
      .get<Product[]>('https://fakestoreapi.com/products')
      .pipe(delay(2000));
  }
  fetchProductById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(
      `https://fakestoreapi.com/products/${id}`
    );
  }
}
