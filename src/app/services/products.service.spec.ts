import { TestBed, inject } from '@angular/core/testing';
// import { HttpEvent, HttpEventType } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';

import { ProductService } from './products.service';
import { Product } from './Product';

describe('ProductService', () => {
  // blocco di test, con describe si scrive il nome del test
  beforeEach(() => {
    // primo a runnare, testbe dice di configurare il servizio
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProductService],
    });
  });
  // it ci dice di testare il metodo fetchProducts, con le classi di testering
  // service è il parametro che passiamo al metodo
  // it sono mini test, dove inietto con inject i singoli moduli che voglio testare
  it('should be created', inject(
    [ProductService],
    (service: ProductService) => {
      expect(service).toBeTruthy();
    }
  ));
  it('should get products', inject(
    [HttpTestingController, ProductService],
    (httpMock: HttpTestingController, ProductService: ProductService) => {
      //da rinominare in mockData, expecteddata, productslist, ecc
      // non è una lista di prodotti, ma una lista di oggetti
      const expectedProducts = [
        {
          id: 12,
          title: 'pomodoro',
          price: 200,
          description: 'description',
          image: ' https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        },

        {
          id: 10,
          title: 'mmmhLoris',
          price: 300,
          description: 'description2',
          image: ' https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg',
        },
      ];

      ProductService.fetchProducts().subscribe((data: Product[]) => {
        // ci aspettiamo che il servizio restituisca i dati uguali a quelli mockati
        expect(data).toEqual(expectedProducts);
      });
      // con httpMock.expectOne() stiamo controllando che la chiamata sia effettuata
      // con il metodo GET e che la risposta sia stata ricevuta
      // dicendoci che la risposta deve essere di tipo OK
      const req = httpMock.expectOne('https://fakestoreapi.com/products');
      expect(req.request.method).toBe('GET');
      // con req.flush() passiamo i dati mockati della riga 57
      // come risposta alla chiamata che abbiamo fatto
      // così il servizio restituisce i dati mockati e il test passa
      req.flush(expectedProducts);
      // con httpMock.verify() controlliamo che la chiamata sia stata effettuata
      httpMock.verify();
    }
  ));
});
